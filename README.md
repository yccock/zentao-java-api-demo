# ZentaoJavaApiDemo

### 介绍
Java对接禅道API示例，Java版本1.8，对接禅道最低开源10.0版本，开发者使用的是专业版8.8，此API运行良好，需注意，禅道开源版、专业版、企业版、集团版等各个版本分别进行版本编码，可在官方动态处查阅版本发布及对应关系。https://www.zentao.net/book/zentaopmshelp/integration-287.html

### 软件架构
简单的Maven项目示例

### 使用说明

与禅道对接主要有两种方式

#### 方式一

​	先请求get-session.json地址，获取session，而后带着此session调用用户登录接口，实现用户登录，之后可以通过http请求实现对应的数据增改操作，禅道中会正确记录此用户。

参见禅道官方文档链接：https://devel.easycorp.cn/book/extension/intro-45.html

#### 方式二

​	注册为扩展应用，通过应用名和应用密钥进行登录，可选择免密登录，适用于获取禅道数据的场景。

注：对于部分接口，可以传入操作人姓名的，从而通过API来模拟用户操作。

参见禅道官方文档链接：https://www.zentao.net/book/zentaopmshelp/integration-287.html

### TODO

- [ ] 更新代码说明
- [ ] 补充方式一示例
- [ ] 增加post请求

### 联系方式

E-mail: diyuchen@esunny.cc

QQ: 289686387(晨鼠)

Tel&WeChat: 13283888398

地址：河南省郑州市金水区龙湖外环东路31号郑州商品交易所技术中心

> 郑州易盛信息技术有限公司 测试中心 B416 效能组 狄雨晨