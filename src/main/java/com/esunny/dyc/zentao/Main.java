package com.esunny.dyc.zentao;

import com.esunny.dyc.zentao.proxy.Constant;
import com.esunny.dyc.zentao.proxy.application.ApiAdapter;

import java.util.LinkedHashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args){
        Map<String,String> paramMap = new LinkedHashMap<>();
        paramMap.put(ApiAdapter.ZENTAO_MODULE_TAG,"bug");
        paramMap.put(ApiAdapter.ZENTAO_FUNCTION_TAG,"view");
        //设置请求参数id为1
        paramMap.put("id","1");
        ApiAdapter.get(paramMap);
    }
}
